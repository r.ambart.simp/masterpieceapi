<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Repository\SocietyRepository;
use App\Form\SocietyType;
use App\Entity\Society;

/**
 * @Route("/api/society", name="society")
 */
class SocietyController extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;
 
    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }
    /**
     * @Route(methods="GET")
     */
    public function index(SocietyRepository $repo)
    {
        $soc = $repo->findAll();
        $json = $this->serializer->serialize($soc, 'json');


        return new JsonResponse($json, 200, [], true);

    }
    /**
     * @Route(methods="POST")
     */
    public function add(Request $request, ObjectManager $manager) {
        $soc = new Society();     
        $form = $this->createForm(SocietyType::class, $soc);

        $form->submit(json_decode($request->getContent(), true));

        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($soc);
            $manager->flush();

            return new JsonResponse($this->serializer->serialize($soc, 'json'), 201, [], true);
        }

        return $this->json($form->getErrors(true), 400);

    }

    /**
     * @Route("/{society}", methods="GET")
     */
    public function one(Society $society) {
        return new JsonResponse($this->serializer->serialize($society, 'json'), 200, [], true);
    }

    /**
     * @Route("/{id}", methods="PATCH")
     */
    public function PatchSociety(Society $society, Request $request, ObjectManager $manager) {
       

        $form = $this->createForm(SocietyType::class, $society);

        $form->submit(json_decode($request->getContent(), true), false);

        if($form->isSubmitted() && $form->isValid()) {

            $manager->flush();

            return new JsonResponse($this->serializer->serialize($society, 'json'), 200, [], true);
        }

        return $this->json($form->getErrors(true), 400);
    
}
}