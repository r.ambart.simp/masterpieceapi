<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;
use App\Form\UserType;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\Serializer\SerializerInterface;

class UserController extends AbstractController
{
    /**
     * @var JMSSerializerInterface
     */
    private $serializer;
    /**
     * On injecte le JMS\Serializer dans le constructeur, car on en aura
     * besoin dans toutes les méthodes de ce contrôleur
     */
    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/register", methods="POST")
     */
    public function register(Request $request, ObjectManager $manager) {
        $user = new User();     
        $form = $this->createForm(UserType::class, $user);

        $form->submit(json_decode($request->getContent(), true));

        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($user);
            $manager->flush();

            return new JsonResponse($this->serializer->serialize($user, 'json'), 201, [], true);
        }

        return $this->json($form->getErrors(true), 400);

    }
    /**
     * @Route("/profil/{user}", methods="GET")
     */

    public function onePerson(User $user =null) {
        if(!$user) {
            $user = $this->getUser();
        }
        return new JsonResponse($this->serializer->serialize($user, 'json'), JsonResponse::HTTP_OK, [], true);
    }
}
