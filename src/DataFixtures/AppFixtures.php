<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use App\Entity\Society;
use Symfony\Component\Validator\Constraints\DateTime;

class AppFixtures extends Fixture
{
    public function loadUser(ObjectManager $manager)
    {

        // $user = new User();
        // $user->setUsername('Admin');
        // $user->setName('Raphou');
        // $user->setSurname('Raph');
        // $user->setPassword('admin');
        // $user->setRoles(['ROLE_ADMIN']);

        
        // $manager->persist($user);
        // $manager->flush();
    }
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('test');
        $user->setName('test');
        $user->setSurname('test');
        $user->setPassword('admin');
        $user->setRoles(['ROLE_ADMIN']);
        $user->setEmail('test@test.com');
        $manager->persist($user);
         
        $user2 = new User();
        $user2->setUsername('admin');
        $user2->setName('admin');
        $user2->setSurname('admin');
        $user2->setPassword('admin');
        $user2->setRoles(['ROLE_ADMIN']);
        $user2->setEmail('admin@admin.com');
        $manager->persist($user2);

        $soc = new Society();
        $soc->setDescription('Acti');
        $soc->setUserid($user2);
        
        $soc->setCreationdate(new \DateTime('12-12-1994'));
        $soc->setName('Acti');
        $soc->setType('SNN');
        $soc->setContent('acti est une agence conseil indépendante qui accélère votre transformation digitale');
        $manager->persist($soc);

    
        $soc2 = new Society();
        $soc2->setDescription('Sogilis');
        $soc2->setUserid($user2);
        $soc2->setCreationdate(new \DateTime('12-12-1994'));
        $soc2->setName('Sogilis');
        $soc2->setType('SNN');
        $soc2->setContent('Sogilis est une agence accélère votre transformation digitale');
        
        $manager->persist($soc2);
        $manager->flush();
    }
}
