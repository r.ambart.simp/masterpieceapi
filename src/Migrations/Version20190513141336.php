<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190513141336 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE comment CHANGE userid_id userid_id INT DEFAULT NULL, CHANGE societyid_id societyid_id INT DEFAULT NULL');
        $this->addSql('DROP INDEX UNIQ_8D93D649F85E0677 ON user');
        $this->addSql('ALTER TABLE user ADD email VARCHAR(255) NOT NULL, ADD age INT DEFAULT NULL, DROP username, DROP roles, DROP name, DROP surname, DROP address');
        $this->addSql('ALTER TABLE society CHANGE userid_id userid_id INT DEFAULT NULL, CHANGE creationdate creationdate DATETIME DEFAULT NULL, CHANGE employes employes INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE comment CHANGE userid_id userid_id INT DEFAULT NULL, CHANGE societyid_id societyid_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE society CHANGE userid_id userid_id INT DEFAULT NULL, CHANGE creationdate creationdate DATETIME DEFAULT \'NULL\', CHANGE employes employes INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD username VARCHAR(180) NOT NULL COLLATE utf8mb4_unicode_ci, ADD roles LONGTEXT NOT NULL COLLATE utf8mb4_bin, ADD surname VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD address VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, DROP age, CHANGE email name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649F85E0677 ON user (username)');
    }
}
